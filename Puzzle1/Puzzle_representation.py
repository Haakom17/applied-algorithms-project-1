import numpy as np
import random


class sudokuBoard:

    def __init__(self):
        self.board = np.zeros((9, 9))


def promising(board, x, y, n):
    if x < 3:
        nx = 3
    elif x < 6:
        nx = 6
    else:
        nx = 9
    if y < 3:
        ny = 3
    elif y < 6:
        ny = 6
    else:
        ny = 9
    column = board[x, :]
    row = board[:, y]
    neighborhood = board[:, range(ny - 3, ny)][range(nx - 3, nx), :]
    if n not in column and n not in row and n not in neighborhood:
        return True
    else:
        return False




#def solver(board, x=0, y=0, n=1):
#    if 0 not in board:
#        print(board)
#        return
#    if promising(board, x, y, n):
#        board[x, y] = n
#        if x < 8:
#            solver(board, x+1, y, 1)
#        elif y < 8:
#            solver(board, 0, y + 1, 1)
#    elif n < 9:
#        solver(board, x, y , n+1)
#    elif x > 0:
#        solver(board, x-1, y, board[x-1, y]+1)
#    else:
#        solver(board, 8, y-1, board[8, y-1] + 1)
count = 0
visited = 0

def solver(board, x=0, y=0):
    while board[x, y] != 0:
        if x == 8:
            if y == 8:
                print(board)
                return True
            y = y + 1
            x = 0
        else:
            x+=1

    for n in range(1, 10):
        global visited
        visited = visited + 1
        if promising(board, x, y, n):
            board[x, y] = n
            global count
            count = count + 1
            if x == 8:
                if y == 8:
                    print(board)
                    return True
                if solver(board, 0, y+1):
                    return True
            else:
                if solver(board, x+1, y):
                    return True
    board[x, y] = 0
    return False




newSudoku = sudokuBoard()
solver(newSudoku.board)
print(count)
print(visited)
print(newSudoku.board)

game = np.array([[0,0,0,7,0,5,0,8,0],
        [7,0,5,0,0,1,3,0,0],
        [4,6,0,0,2,0,0,0,7],
        [5,0,7,0,8,0,0,0,0],
        [2,0,3,0,1,0,4,0,5],
        [0,0,0,0,3,0,8,0,1],
        [6,0,0,0,5,0,0,4,8],
        [0,0,4,1,0,0,9,0,2],
        [0,2,0,4,0,6,0,0,0]])
count = 0
visited = 0
solver(game)
print(count)
print(visited)

game = np.array([
        [0,0,8,0,0,9,0,0,3],
        [5,0,0,1,0,0,0,4,0],
        [0,0,0,0,8,7,5,0,0],
        [0,1,0,0,5,0,3,0,0],
        [0,4,0,0,0,0,0,6,0],
        [0,0,6,0,7,0,0,1,0],
        [0,0,7,9,6,0,0,0,0],
        [0,9,0,0,0,4,0,0,6],
        [2,0,0,3,0,0,9,0,0]])
count = 0
visited = 0
solver(game)
print(count)
print(visited)

game1 = np.array([
        [9, 0, 4, 0, 2, 7, 0, 0, 1],
        [0, 0, 0, 0, 0, 4, 0, 8, 0],
        [0, 0, 0, 0, 0, 6, 9, 7, 0],
        [8, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 7, 0, 0, 0, 3, 6, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 9],
        [0, 3, 8, 7, 0, 0, 0, 0, 0],
        [0, 9, 0, 4, 0, 0, 0, 0, 0],
        [1, 0, 0, 6, 8, 0, 4, 0, 2]])

count = 0
visited = 0
solver(game1)
print(count)
print(visited)