import numpy as np

# Positive Constraint
top = [1, -1, -1, 2, 1, -1]
left = [2, 3, -1, -1, -1]

# Negative Constraint
bottom = [2, -1, -1, 2, -1, 3]
right = [-1, -1, -1, 1, -1]

# Piece orientation
rules = [["L", "R", "L", "R", "T", "T"],
         ["L", "R", "L", "R", "B", "B"],
         ["T", "T", "T", "T", "L", "R"],
         ["B", "B", "B", "B", "T", "T"],
         ["L", "R", "L", "R", "B", "B"]
         ]

emptyBoard = np.zeros((5, 6), dtype=object)

visitedNodes = 0
promisingNodes = 0


def checkNode(board, row, column):

    # Count visited nodes
    global promisingNodes
    global visitedNodes
    visitedNodes += 1

    # print(board)
    # print("\n")

    if checkSolution(board):
        print(board)
        print(f"visited nodes: {visitedNodes}")
        print(f"Promising nodes: {promisingNodes}")


    # jump to next row
    if column == 6:
        checkNode(board, row + 1, 0)

    elif row < 5:
        # Horizontal magnet placement
        if rules[row][column] == "L":
            # Option 1 (+-)
            if promising(board, row, column, "+"):
                if promising(board, row, column + 1, "-"):
                    promisingNodes += 1

                    board[row][column] = "+"
                    board[row][column + 1] = "-"

                    checkNode(board, row, column + 2)

                    board[row][column] = 0
                    board[row][column + 1] = 0

            # Option 2 (-+)
            if promising(board, row, column, "-"):
                if promising(board, row, column + 1, "+"):
                    promisingNodes += 1

                    board[row][column] = "-"
                    board[row][column + 1] = "+"

                    checkNode(board, row, column + 2)

                    board[row][column] = 0
                    board[row][column + 1] = 0

            # Option 3 (XX)
            promisingNodes += 1
            board[row][column] = "X"
            board[row][column + 1] = "X"

            checkNode(board, row, column + 2)

            board[row][column] = 0
            board[row][column + 1] = 0

        # Vertical magnet placement
        elif rules[row][column] == "T":
            # Option 1 (+-)
            if promising(board, row, column, "+"):
                if promising(board, row + 1, column, "-"):
                    promisingNodes += 1

                    board[row][column] = "+"
                    board[row + 1][column] = "-"

                    checkNode(board, row, column + 1)

                    board[row][column] = 0
                    board[row + 1][column] = 0

            # Option 2 (-+)
            if promising(board, row, column, "-"):
                if promising(board, row + 1, column, "+"):
                    promisingNodes += 1

                    board[row][column] = "-"
                    board[row + 1][column] = "+"

                    checkNode(board, row, column + 1)

                    board[row][column] = 0
                    board[row + 1][column] = 0

            # Option 3 (xx)
            promisingNodes += 1
            board[row][column] = "X"
            board[row + 1][column] = "X"

            checkNode(board, row, column + 1)

            board[row][column] = 0
            board[row + 1][column] = 0

        else:
            checkNode(board, row, column + 1)




def promising(board, row, column, polarity):

    if not checkAdjacent(board, row, column, polarity):
        return False
    if not checkConstraints(board, row, column, polarity):
        return False

    return True



def checkConstraints(board, row, column, polarity):

    # Positive constraints
    checkTop = [0 for i in range(len(top))]
    checkLeft = [0 for i in range(len(left))]

    # Negative constraints
    checkBottom = [0 for i in range(len(bottom))]
    checkRight = [0 for i in range(len(right))]

    # Update check arrays for comparison
    for r in range(0, 5):
        for c in range(0, 6):
            if board[r][c] == "+":

                checkLeft[r] += 1
                checkTop[c] += 1

            elif board[r][c] == "-":

                checkRight[r] += 1
                checkBottom[c] += 1

    # Check positive constraints
    if polarity == "+":
        # Check top constraints
        if top[column] != -1:
            if checkTop[column] >= top[column]:
                return False
        # Check left constraint
        if left[row] != -1:
            if checkLeft[row] >= left[row]:
                return False

    # Check negative constraints
    if polarity == "-":
        # Check bottom constraints
        if bottom[column] != -1:
            if checkBottom[column] >= bottom[column]:
                return False
        # Check right constraints
        if right[row] != -1:
            if checkRight[row] >= right[row]:
                return False

    return True



def checkAdjacent(board, row, column, polarity):

    # check left square
    if column != 0:
        if board[row][column - 1] == polarity:
            return False

    # check right square
    if column != 5:
        if board[row][column + 1] == polarity:
            return False

    # check square above
    if row != 0:
        if board[row - 1][column] == polarity:
            return False

    # check square below
    if row != 4:
        if board[row + 1][column] == polarity:
            return False

    return True



def checkSolution(board):

    # Positive constraints
    checkTop = [0 for i in range(len(top))]
    checkLeft = [0 for i in range(len(left))]

    # Negative constraints
    checkBottom = [0 for i in range(len(bottom))]
    checkRight = [0 for i in range(len(right))]

    # Update check arrays for comparison
    for r in range(0, 5):
        for c in range(0, 6):
            if board[r][c] == "+":

                checkLeft[r] += 1
                checkTop[c] += 1

            elif board[r][c] == "-":

                checkRight[r] += 1
                checkBottom[c] += 1

    # Check top constraints
    for i in range(len(top)):
        if top[i] != -1:
            if top[i] != checkTop[i]:
                return False

    # Check left constraints
    for i in range(len(left)):
        if left[i] != -1:
            if left[i] != checkLeft[i]:
                return False

    # Check bottom constraints
    for i in range(len(bottom)):
        if bottom[i] != -1:
            if bottom[i] != checkBottom[i]:
                return False

    # Check right constraints
    for i in range(len(right)):
        if right[i] != -1:
            if right[i] != checkRight[i]:
                return False

    return True


checkNode(emptyBoard, 0, 0)



