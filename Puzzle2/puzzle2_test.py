import time

def Up(i):
    return i-2
def Down(i):
    return i+2
def Left(j):
    return j-2
def Right(j):
    return j+2
def up(i):
    return i-1
def down(i):
    return i+1
def left(j):
    return j-1
def right(j):
    return j+1

def solution():
    for row in board:
        if sum(row) != N:
            return 0
    print("Solution found!")
    print(board)
    return 1

def promising(col,row):
    proms = []

    if (Up(col) >= 0 and Up(col) < N) and (left(row) >= 0 and left(row) < N) and board[Up(col)][left(row)] == 0:
	    proms.append([Up(col),left(row)])

    if (Up(col) >= 0 and Up(col) < N) and (right(row) >= 0 and right(row) < N) and board[Up(col)][right(row)] == 0:
	    proms.append([Up(col),right(row)])

    if (Down(col) >= 0 and Down(col) < N) and (left(row) >= 0 and left(row) < N) and board[Down(col)][left(row)] == 0:
	    proms.append([Down(col),left(row)])

    if (Down(col) >= 0 and Down(col) < N) and (right(row) >= 0 and right(row) < N) and board[Down(col)][right(row)] == 0:
        proms.append([Down(col),right(row)])

    if (up(col) >= 0 and up(col) < N) and (Left(row) >= 0 and Left(row) < N) and board[up(col)][Left(row)] == 0:
        proms.append([up(col),Left(row)])

    if (up(col) >= 0 and up(col) < N) and (Right(row) >= 0 and Right(row) < N) and board[up(col)][Right(row)] == 0:
	    proms.append([up(col),Right(row)])

    if (down(col) >= 0 and down(col) < N) and (Left(row) >= 0 and Left(row) < N) and board[down(col)][Left(row)] == 0:
        proms.append([down(col),Left(row)])

    if (down(col) >= 0 and down(col) < N) and (Right(row) >= 0 and Right(row) < N) and board[down(col)][Right(row)] == 0:
	    proms.append([down(col),Right(row)])
    
    return proms

def checknode(col,row):
    vis.append([col,row])
    board[col][row] = 1
    proms = promising(col,row)
    if (solution()):
            print("Promising nodes: ", len(promis))
            print("Visited nodes: ", len(vis))
            exit()
    if(len(proms) > 0):
        for coord in proms:
            promis.append(coord)
        for u in proms:
            checknode(u[0],u[1])
            vis.append(u)
            #time.sleep(0.000001)            #Sleeper function for calming the processor.
        board[col][row] = 0
        return
    else:
        board[col][row] = 0
        return

def heurSolv():
    for row in heurBoard:
        for spot in row:
            if spot == 0:
                return 0
    print("Solution found!")
    print(heurBoard)
    return 1

def heurProm(col,row):
    proms = []

    if (Up(col) >= 0 and Up(col) < N) and (left(row) >= 0 and left(row) < N) and heurBoard[Up(col)][left(row)] == 0:
	    proms.append([Up(col),left(row)])

    if (Up(col) >= 0 and Up(col) < N) and (right(row) >= 0 and right(row) < N) and heurBoard[Up(col)][right(row)] == 0:
	    proms.append([Up(col),right(row)])

    if (Down(col) >= 0 and Down(col) < N) and (left(row) >= 0 and left(row) < N) and heurBoard[Down(col)][left(row)] == 0:
	    proms.append([Down(col),left(row)])

    if (Down(col) >= 0 and Down(col) < N) and (right(row) >= 0 and right(row) < N) and heurBoard[Down(col)][right(row)] == 0:
        proms.append([Down(col),right(row)])

    if (up(col) >= 0 and up(col) < N) and (Left(row) >= 0 and Left(row) < N) and heurBoard[up(col)][Left(row)] == 0:
        proms.append([up(col),Left(row)])

    if (up(col) >= 0 and up(col) < N) and (Right(row) >= 0 and Right(row) < N) and heurBoard[up(col)][Right(row)] == 0:
	    proms.append([up(col),Right(row)])

    if (down(col) >= 0 and down(col) < N) and (Left(row) >= 0 and Left(row) < N) and heurBoard[down(col)][Left(row)] == 0:
        proms.append([down(col),Left(row)])

    if (down(col) >= 0 and down(col) < N) and (Right(row) >= 0 and Right(row) < N) and heurBoard[down(col)][Right(row)] == 0:
	    proms.append([down(col),Right(row)])
    
    return proms

def heuristic(col,row,move):
    vis.append([col,row])
    move += 1
    heurBoard[col][row] = move

    proms = heurProm(col,row)
    if (heurSolv()):
            print("Visited nodes: ", len(vis))
            exit()
    if(len(proms) > 0):
        heurProms = []
        for u in proms:
            check = heurProm(u[0],u[1])
            heurProms.append(check)
        for i in range(len(heurProms)):
            if len(heurProms[i]) == []:
                del heurProms[i]
                del proms[i]
        
        n = len(heurProms)
        for i in range(n-1):
            for j in range(n-i-1):
                if len(heurProms[j]) > len(heurProms[j+1]):
                    heurProms[j], heurProms[j+1] = heurProms[j+1], heurProms[j]
                    proms[j], proms[j+1] = proms[j+1], proms[j]

        for u in proms:
            heuristic(u[0],u[1],move)
            vis.append(u)
        heurBoard[col][row] = 0
        return
    else:
        heurBoard[col][row] = 0
        return


N = 8
vis = []
promis = []
board = [[0 for j in range(N)] for i in range(N)]
heurBoard = [[0 for j in range(N)] for i in range(N)]
#checknode(0,0)
print(heurBoard)
move = 0
heuristic(0,0,move)
