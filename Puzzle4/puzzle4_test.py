
N = 2
output = list()
solv = list()

def testSolver():

    print(output)
    
    n = range(2^(N-1)-1)
    for i in n:
        for j in n:
            for h in n:
                output.append([i,j,h])

    print(output)
    
    #sorting
    n = len(output)
    for i in range(n-1):
        for j in range(n-1):
            if sum(output[j]) == sum(output[j+1]):
                output[j+1],output[j+2] = output[j+2],output[j+1]

    for i in output:
        solv.append(sum(i))

    print(solv)

    pass

testSolver()