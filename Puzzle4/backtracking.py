
codeList = []

def generator(N):
    string = []
    for i in range(0, N):
        string.append("0")
    shift(string, 0, [string])

count = 0
visited = 0

def shift(string, index=0, codeList = []):
    global count
    global visited
    if len(codeList) == 2**(len(string)):
        for entry in codeList:
            print("".join(entry) + " ")
        print(count)
        print(visited)
    for i in range(len(string)-1,-1,-1):
        newstring = list(string)
        if i == index:
            pass
            visited += 1
        elif newstring[i] == "0":
            newstring[i] = "1"
            visited += 1
        elif newstring[i] == "1":
            newstring[i] = "0"
            visited += 1
        if newstring not in codeList:
            count = count + 1
            visited += 1
            newvariable = newstring
            codeList.append(list(newstring))
            shift(newstring, i, codeList)
        else:
            newstring = string

generator(4)
count = 0
visited = 0

shift(["0","0","0"])

