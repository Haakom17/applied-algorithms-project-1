
N = 2

def Solver():

    o = list()
    done = list()
    solv = list()
    o.append('0')
    o.append('1')

    #the loops generates 2*i amount of codes,
    #   based on the previous (i) amount of codes.
    k = 2
    #shifts the value in N left, pushing leftmost bits out.
    breaker = 1<<N

    while(1):

        #if-breaker:
        if k >= breaker:
            break
        
        #appending the reversed order.
        for i in range(k-1,-1,-1):
            o.append(o[i])

        #appending 0 to the first half.
        for i in range(k):
            o[i] = '0' + o[i]
        
        #appending 1 to the second half.
        for i in range(k, 2*k):
            o[i] = '1' + o[i]
        
        #shifts bit for the breaker at the top of the while-loop.
        k = k << 1

    #check if solved
    #for i in o:
    #    solv.append([int(i,2)])
    #for i in solv:
    #    done.append(sum(i))
    print(o)
    #print(solv)
    #print(done)

Solver()